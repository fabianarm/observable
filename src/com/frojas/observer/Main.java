package com.frojas.observer;
import com.frojas.observer.classes.*;


public class Main {
  public static void main(String[] args) {
    BeingObserved beingObserved = new BeingObserved();

    EmailNotification email = new EmailNotification();
    SmsNotification sms = new SmsNotification();
    Logger logger = new Logger();

    beingObserved.addObserver(logger);
    beingObserved.addObserver(email);
    beingObserved.addObserver(sms);

    beingObserved.proccessApplication();
  }
}
