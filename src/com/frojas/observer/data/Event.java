package com.frojas.observer.data;

public class Event {

  private String message;
  private String author;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  @Override
  public String toString() {
    return String.format("Author: %s \nMessage: %s", this.author, this.message);
  }
}
