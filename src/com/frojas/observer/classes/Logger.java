package com.frojas.observer.classes;

import java.util.Observable;
import java.util.Observer;

public class Logger implements Observer {
  public Logger() {
  }

  public void update(Observable o, Object arg) {
    System.out.println(o.toString());
    System.out.println(arg.toString());
    this.logMessage();
  }

  public void logMessage() {
    System.out.println("Se esta logenado el mensaje.");
    System.out.println("\n");
  }
}