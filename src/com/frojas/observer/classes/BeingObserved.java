package com.frojas.observer.classes;

import com.frojas.observer.data.Event;
import java.util.Observable;
import java.util.Observer;

public class BeingObserved extends Observable {

  public BeingObserved() {
    super();
  }

  @Override
  public synchronized void addObserver(Observer o) {
    super.addObserver(o);
  }

  @Override
  public synchronized void deleteObserver(Observer o) {
    super.deleteObserver(o);
  }

  @Override
  public void notifyObservers() {
    super.notifyObservers();
  }

  @Override
  public void notifyObservers(Object arg) {
    super.notifyObservers(arg);
  }

  @Override
  public synchronized void deleteObservers() {
    super.deleteObservers();
  }

  @Override
  protected synchronized void setChanged() {
    super.setChanged();
  }

  @Override
  protected synchronized void clearChanged() {
    super.clearChanged();
  }

  @Override
  public synchronized boolean hasChanged() {
    return super.hasChanged();
  }

  @Override
  public synchronized int countObservers() {
    return super.countObservers();
  }

  public void proccessApplication() {
    this.setChanged();
    System.out.println("proccessApplication");
    Event event = new Event();
    event.setAuthor("BeingObserved");
    event.setMessage("Se esta procesando la aplicación");
    this.notifyObservers(event);
  }
}
