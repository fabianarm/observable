package com.frojas.observer.classes;

import java.util.Observable;
import java.util.Observer;

public class EmailNotification implements Observer {
  public EmailNotification() {
  }

  public void update(Observable o, Object arg) {
    System.out.println(o.getClass().toString());
    System.out.println(arg.toString());
    this.SendEmail();
  }

  public void SendEmail() {
    System.out.println("Se esta enviando el email");
    System.out.println("\n");
  }
}
