package com.frojas.observer.classes;

import java.util.Observable;
import java.util.Observer;

public class SmsNotification implements Observer {
  public SmsNotification() {
  }

  public void update(Observable o, Object arg) {
    System.out.println(o.toString());
    System.out.println(arg.toString());

    this.smsNotification();
  }

  public void smsNotification() {
    System.out.println("Se esta enviando el correo electronico.");
    System.out.println("\n");
  }
}
